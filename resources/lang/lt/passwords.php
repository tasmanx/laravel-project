<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Slaptažodis turi būti bent 6 simbolių!',
    'reset' => 'Tavo slaptažodis buvo atstatytas!',
    'sent' => 'Nusiuntėme tau elektroninį laišką su slaptažodžio atstatymo nuoroda!',
    'token' => 'Slaptažodžio atstatymo token yra neteisingas',
    'user' => "Negalime surasti vartotojo su tokiu e-mail adresu.",

];
