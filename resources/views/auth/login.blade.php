@extends('main')

@section('title', '| Prisijungimas')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            {!! Form::open() !!}

                {{ Form::label('email', 'Email')}}
                {{ Form::email('email', null, ['class' => 'form-control margin-bottom']) }}

                {{ Form::label('password', 'Slaptažodis') }}
                {{ Form::password('password', ['class' => 'form-control margin-bottom']) }}

                {{ Form::checkbox('remember') }} {{ Form::label('remember', 'Įsiminti')}}

                {{ Form::submit('Prisijungti', ['class' => 'btn btn-primary btn-block margin-bottom']) }}

                <p><a href="{{ url('password/reset') }}">Užmiršau slaptažodį</a></p>

            {!! Form::close() !!}
        </div>
    </div>

@endsection
