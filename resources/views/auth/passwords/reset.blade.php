@extends('main')

@section('title', ' | Pamiršau slaptažodį')
@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Atkurti slaptažodį</div>

                <div class="panel-body">
                    {!! Form::open(['url' => 'password/reset', 'method' => 'POST']) !!}

                    {{ Form::hidden('token', $token) }}

                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email', $email, ['class' => 'form-control margin-bottom']) }}

                    {{ Form::label('password', 'Naujas slaptažodis') }}
                    {{ Form::password('password', ['class' => 'form-control margin-bottom']) }}

                    {{ Form::label('password_confirmation', 'Pakartokite slaptažodį') }}
                    {{ Form::password('password_confirmation', ['class' => 'form-control margin-bottom']) }}

                    {{ Form::submit('Atkurti slaptažodį', ['class' => 'btn btn-primary']) }}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
