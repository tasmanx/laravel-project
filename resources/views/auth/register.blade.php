@extends('main')

@section('title', '| Prisijungimas')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            {!! Form::open() !!}

                {{ Form::label('name', 'Vardas')}}
                {{ Form::text('name', null, ['class' => 'form-control margin-bottom']) }}

                {{ Form::label('email', 'Email')}}
                {{ Form::email('email', null, ['class' => 'form-control margin-bottom']) }}

                {{ Form::label('password', 'Slaptažodis') }}
                {{ Form::password('password', ['class' => 'form-control margin-bottom']) }}

                {{ Form::label('password_confirmation', 'Pakartokite slaptažodį') }}
                {{ Form::password('password_confirmation', ['class' => 'form-control margin-bottom']) }}

                {{ Form::submit('Registruoti', ['class' => 'btn btn-primary btn-block']) }}

            {!! Form::close() !!}
        </div>
    </div>

@endsection
