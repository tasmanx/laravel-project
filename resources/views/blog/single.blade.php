@extends('main')

@section('title', "| $post->title")

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>{{ $post->title }}</h1>
            <p>{{ $post->description }}</p>



            <div class="col-sm-6">
              {!! Html::linkRoute('blog.index', 'Atgal', array($post->id) ,array('class' => 'btn btn-danger')) !!}
              @if (Storage::disk('local')->has($post->slug . '-' . $post->user_id . '.rar'))

              <a class="btn btn-success" href="{{ route('file', ['fileName' => $post->slug . '-' . $post->user_id . '.rar']) }}" download >Parsisiųsti</a>
              @endif
            </div>
        </div>
    </div>

@endsection
