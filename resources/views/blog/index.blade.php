@extends('main')

@section('title', '| Visi Mokslo Darbai')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Visi Mokslo Darbai</h1>
        </div>
    </div>

    @foreach ($posts as $post)
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2>{{ $post->title }}</h2>
            <h5>Įkelta: {{ date( 'm-j-Y H:i', strtotime($post->created_at)) }}</h5>

            <p> {{ substr($post->description, 0, 300) }} {{ strlen($post->description) > 300 ? "..." : "" }}</p>

            <a href="{{ route('blog.single', $post->slug) }}" class="btn btn-primary">Daugiau</a>
            <hr>
        </div>
    </div>
    @endforeach

    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                {!! $posts->links() !!}
            </div>
        </div>
    </div>

@endsection
