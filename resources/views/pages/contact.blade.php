@extends('main')

@section('title', ' | Contacts')
@section('content')
      <div class="row">
        <div class="col-md-12">
          <h1>Parašyk man!</h1>
          <hr>
          <form>
            <div class="form-group">
              <label name="email">Email:</label>
              <input id="email" name="email" class="form-control">
            </div>

            <div class="form-group">
              <label name="subject">Tema:</label>
              <input id="subject" name="subject" class="form-control">
            </div>

            <div class="form-group">
              <label name="message">Žinutė:</label>
              <textarea id="message" name="message" class="form-control">Rašykte čia...</textarea>
            </div>

            <input type="submit" value="Siųsti" class="btn btn-success">
          </form>
        </div>
      </div>
@endsection
