@extends('main')

@section('title', ' | Naujienos')
@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="jumbotron">
            <h1>Sveiki atvykę į mokslo darbų svetainę!</h1>
            <p class="lead">Čia gali dalintis savo mokslo darbais su kitais labai
              lengvai. Norėdamas įkelti savo mokslo darbą užsiregistruok!</p>
            <p><a href="{{ route('register') }}" class="btn btn-primary btn-lg">Registruotis</a>
            </p>
          </div>
        </div>
      </div>
      <!-- end of header .row -->

      <div class="row">
        <div class="col-md-8">

          @foreach($posts as $post)

            <div class="post" style="float:right;">
              <h3>{{ $post->title }}</h3>
              <p style="float:right;">{{ substr($post->description, 0, 300) }} {{ strlen($post->description) > 300 ? "..." : "" }}</p>
              <a  href="{{ url('blog/'.$post->slug) }}" class="btn btn-primary">Plačiau</a>
            </div>



          @endforeach

        </div>

        <div class="col-md-3 col-md-offset-1">
          <h2>Reklama</h2>
        </div>
      </div>
@endsection
