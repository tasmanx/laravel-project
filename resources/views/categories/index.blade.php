@extends('main')

@section('title', '| Kategorijos')

@section('content')

    <div class="row">
      <div class="col-md-8">
          <h1>Kategorijos</h1>
          <table class="table">
              <thread>
                  <tr>
                      <th>#</th>
                      <th>Pavadinimas</th>
                      <th></th>
                  </tr>
              </thread>
              <tbody>
                  @foreach($categories as $category)
                  <tr>
                      <th>{{ $category->id }}</th>
                      <td>{{ $category->name }}</td>

                      <td>
                        {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'DELETE']) !!}

                        {!! Form::submit('Ištrinti', ['class' => 'btn btn-danger']) !!}

                        {!! Form::close() !!}
                      </td>
                    </tr>
                  @endforeach
              </tbody>
            </table>
      </div>

      <div class="col-md-3">
          <div class="well">
              {!! Form::open(['route' => 'categories.store']) !!}
              <h2>Nauja kategorija</h2>
              {{ Form::label('name', 'Pavadinimas') }}
              {{ Form::text('name', null, ['class' => 'form-control margin-bottom']) }}

              {{ Form::submit('Sukurti naują', ['class' => 'btn btn-primary btn-block']) }}
              {!! Form::close() !!}
          </div>
      </div>

    </div>

@stop
