@extends('main')

@section('title', '| Įkelti naują mokslo darbą')

@section('stylesheets')

  {!! Html::style('css/parsley.css') !!}

@endsection

@section('content')

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h1>Įkelti naują mokslo darbą</h1>
      <hr>
      {!! Form::open(array('route' => 'posts.store', 'data-parsley-validate' => '', 'enctype' => 'multipart/form-data' )) !!}
        {{ Form::label('title', 'Pavadinimas') }}
        {{ Form::text('title', null, array('class' => 'form-control margin-bottom', 'required' => '', 'minlength' => '5', 'maxlength' => '255')) }}

        {!! Form::label('slug', 'Šliuzas') !!}
        {!! Form::text('slug', null, array('class' => 'form-control margin-bottom', 'required' => '', 'minlength' => '5', 'maxlength' => '255')) !!}

        {{ Form::label('description', 'Aprašymas') }}
        {{ Form::textarea('description', null, array('class' => 'form-control margin-bottom',
        'required' => '', 'minlength' => '10' )) }}

        {!! Form::label('file', 'Pasirinkti failą (.rar)') !!}
        {!! Form::file('file', null, array('class' => 'form-control margin-bottom', 'required' => '')) !!}

        <!--
        {!! Form::label('image', 'Pasirinkti paveiksliuką') !!}
        {!! Form::file('image', null, array('class' => 'form-control' )) !!}

        {!! Form::label('file', 'Pasirinkti failą') !!}
        {!! Form::file('file', null, array('class' => 'form-control')) !!}
        -->
        {{ Form::submit('Įkelti darbą' , array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
        <div class="row">
            <div class="col-md-12">
                {{ Html::linkRoute('posts.index', 'Atgal', [], ['class' => 'btn btn-lg btn-danger btn-block margin-top']) }}
            </div>
        </div>
      {!! Form::close() !!}
    </div>
  </div>

@endsection

@section('scripts')
  {!! Html::script('js/parsley.min.js') !!}
