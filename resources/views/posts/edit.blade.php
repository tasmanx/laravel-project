@extends('main')

@section('title', '| Modifikuoti Darbą')

@section('content')

  <div class="row">
    {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT']) !!}
    <div class="col-md-8">
      {{ Form::label('title', 'Pavadinimas') }}
      {{ Form::text('title', null, ['class' => 'form-control input-lg margin-bottom']) }}

      {{ Form::label('slug', 'Url') }}
      {{ Form::text('slug', null, ['class' => 'form-control margin-bottom']) }}

      {{ Form::label('description', 'Aprašymas') }}
      {{ Form::textarea('description', null, ['class' => 'form-control']) }}

      {!! Form::label('file', 'Pasirinkti failą') !!}
      {!! Form::file('file', null, array('class' => 'form-control margin-bottom')) !!}

    </div>

    <div class="col-md-4">
      <div class="well">
        <dl>
          <dt>Sukurta:</dt>
          <dd> {{ date( 'm-j-Y H:i', strtotime($post->created_at)) }}</dd>
        </dl>

        <dl>
          <dt>Modifikuota:</dt>
          <dd> {{ date( 'm-j-Y H:i', strtotime($post->updated_at)) }}</dd>
        </dl>
        <hr>

        <div class="row">
          <div class="col-sm-6">
            {!! Html::linkRoute('posts.show', 'Atgal', array($post->id) ,array('class' => 'btn btn-danger btn-block')) !!}
          </div>
          <div class="col-sm-6">
            {{ Form::submit('Išsaugoti', ['class' => 'btn btn-success btn-block']) }}

          </div>
        </div>

      </div>
    </div>
    {!! Form::close() !!}
  </div>


@endsection
