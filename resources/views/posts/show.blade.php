@extends('main')

@section('title', '| Mokslo darbai')

@section('content')

  <div class="row">
    <div class="col-md-8">
      <h1>{{ $post->title }}</h1>

      <p class="">{{ $post->description }}</p>
      {{ $post->image }}
      <p class="">{{ $post->file }}</p>

      @if (Storage::disk('local')->has($post->slug . '-' . $post->user_id . '.rar'))

      <a class="btn btn-success" href="{{ route('file', ['fileName' => $post->slug . '-' . $post->user_id . '.rar']) }}" download >Parsisiųsti</a>
      @endif
    </div>

    <div class="col-md-4">
      <div class="well">
        <dl>
          <dt>Url:</dt>
          <!--or use this:  url('blog/'.$post->slug)-->
          <dd><a href="{{ route('blog.single', $post->slug) }}"> {{ route('blog.single', $post->slug) }}</a></dd>
        </dl>

        <dl>
          <dt>Sukurta:</dt>
          <dd> {{ date( 'm-j-Y H:i', strtotime($post->created_at)) }}</dd>
        </dl>

        <dl>
          <dt>Modifikuota:</dt>
          <dd> {{ date( 'm-j-Y H:i', strtotime($post->updated_at)) }}</dd>
        </dl>
        <hr>

        <div class="row">
          <div class="col-sm-6">
            {!! Html::linkRoute('posts.edit', 'Modifikuoti', array($post->id) ,array('class' => 'btn btn-primary btn-block')) !!}
          </div>
          <div class="col-sm-6">
            {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE']) !!}

            {!! Form::submit('Ištrinti', ['class' => 'btn btn-danger btn-block']) !!}

            {!! Form::close() !!}
          </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Html::linkRoute('posts.index', 'Atgal', [], ['class' => 'btn btn-default btn-block margin-top']) }}
            </div>
        </div>

      </div>
    </div>
  </div>


@endsection
