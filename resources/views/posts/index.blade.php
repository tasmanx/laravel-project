@extends('main')

@section('title', '| Mano mokslo darbai')

@section('content')

    <div class="row">
      <div class="col-md-10">
          <h1>Mano mokslo darbai</h1>
      </div>

      <div class="col-md-2">
          <a href="{{ route ('posts.create')}}" class="btn btn btn-lg btn-block btn-primary btn-h1-spacing">Įkelti naują</a>
      </div>

      <div class="col-md-12">
          <hr>
      </div>

    </div><!-- end of the .row -->

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thread>
                      <th>#</th>
                      <th>Pavadinimas</th>
                      <th>Aprašymas</th>
                      <th>Įkėlimo data</th>
                      <th></th>
                </thread>

                <tbody>

                  @foreach ($posts as $post)
                      <tr>
                          <th>{{ $post->id }}</th>
                          <td>{{ substr($post->title, 0, 30) }}</td>
                          <td>{{ substr($post->description, 0, 70) }} {{ strlen($post->description) > 70 ? "..." : "" }}</td>
                          <td>{{ date( 'm-j-Y H:i', strtotime($post->created_at)) }}</td>
                          <td><a href="{{ route('posts.show', $post->id) }}" class="btn btn-sm btn-default">Peržiūrėti</a>
                            <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-sm btn-default">Modifikuoti</a></td>
                      </tr>
                  @endforeach
                </tbody>
            </table>

            <div class="text-center">
              {!! $posts->links() !!}

            </div>
        </div>

    </div>
@stop
